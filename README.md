# ForeignExchange

It's a recruitment task for Python Developer role in SunScrapers.


## Important Note

This is my second take on this task.
At first I wasted most time on:
- using Community Edition of PyCharm instead of Professional
- working without a Debugger
- implementing functional tests in Selenium
- planing application while writing code

In 3 hours I've done almost nothing.

This time I've already spend some time planning everything as I should've done in the first place.
I would count it as **one hour** of work till this first initial commit.

## Description

Scrapper RSS for reading and providing foreign exchanges rates from European Central Bank.

>Django reads RSS feeds taken from https://www.ecb.europa.eu/home/html/rss.en.html and present them over REST API.

## Architecture

1. Django
1. DRF (Browsable API)
1. SQLite DataBase (Normally would be PostgreSQL)
1. Management script for updating rates on daily basis

#### Libraries

* Django Rest Framework https://www.django-rest-framework.org
* feedparser https://github.com/kurtmckee/feedparser

## Data Base Structure

#### Currency
- code (Char)

#### Exchange
- base (Currency)
- target (Currency)
- rss_url (Char)

#### ExchangeRate
- exchange (Exchange)
- rate (Decimal)
- updated (Datetime)

## JSON Response Structure

```
[
  {
    "base_currency": "EUR",
    "target_currency": "PLN",
    "rates": [
      {
        "rate": 4.3307,
        "updated": "2019-02-15T13:15:00Z"
      },
      {
        "rate": 4.3382,
        "updated": "2019-02-14T13:15:00Z"
      }
    ]
  },
  {
    "base_currency": "EUR",
    "target_currency": "USD",
    "rates": [
      {
        "rate": 1.126,
        "updated": "2019-02-15T13:15:00Z"
      },
      {
        "rate": 1.1268,
        "updated": "2019-02-14T13:15:00Z"
      }
    ]
  }
]
```

## Plan

Write Test and Code + Refactor for

1. Displaying Browsable API (DRF)
1. Getting Data from RSS
1. Saving Data to Database
1. Retrieving Data from API

## Installation

Requirements

* `python >= 3.6`

Installing and setting up project

```
pip install -r requirements.txt; ./manage.py makemigrations; ./manage.py migrate;
```

## Running server

```
./manage.py runserver
```

## Running tests

```
./manage.py test
```

## Manual testing

After running server just go to:

[http://127.0.0.1:8000/api/v1/](http://127.0.0.1:8000/api/v1/)

[http://127.0.0.1:8000/api/v1/?format=json](http://127.0.0.1:8000/api/v1/?format=json)

---

## Afterthoughts

#### What left

1. Implement method(s) for adding Models to Database
1. Writing tests and code for Retrieving Data from API
1. Creating management script for updating database ones in a while
    * Adding all RSS urls from European Central Bank to constants.py

#### Improvements

1. Writing more tests
1. Showing just code for base_currency and target_currency in API response (as updated)