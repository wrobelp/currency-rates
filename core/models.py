from django.db import models


class Currency(models.Model):
    """
      Currency model represents target or base currency.
      code for examples: 'PLN', 'EU'
    """
    code = models.CharField(max_length=3, unique=True)

    def __str__(self):
        return self.code


class Exchange(models.Model):
    """
      Exchange model represents exchange currency type like Exchanging from 'EU' to 'PLN'.
      rss_url stores rss feed link for a given currency pair exchange
    """

    class Meta:
        unique_together = ("target_currency", "base_currency", "rss_url")

    base_currency = models.ForeignKey(Currency, on_delete=models.CASCADE, related_name='base_currency')
    target_currency = models.ForeignKey(Currency, on_delete=models.CASCADE, related_name='target_currency')
    rss_url = models.CharField(max_length=300)

    def __str__(self):
        return str(self.base_currency) + ' => ' + str(self.target_currency)


class ExchangeRate(models.Model):
    """
      Exchange Rate model represents one rate for a given exchange pair on a given time (updated)
    """
    exchange = models.ForeignKey(Exchange, on_delete=models.CASCADE)
    rate = models.DecimalField(decimal_places=5, max_digits=10)
    updated = models.DateTimeField()

    def __str__(self):
        return str(self.exchange) + ' rate: ' + str(self.rate)
