from django.test import TestCase

from core.utils import get_currency_rates, parse_currency_rate_data


class GettingDataFromRSSTest(TestCase):
    """
    Testing getting data from RSS
    """

    def setUp(self):
        # Example ECB rss feed
        self.rss_url = 'https://www.ecb.europa.eu/rss/fxref-pln.html'
        # Minimal mock for retrieved rss response
        self.piece_of_currency_rate_data = {
            'updated': '2019-02-19T14:15:00+01:00',
            'cb_targetcurrency': 'PLN',
            'cb_exchangerate': '4.3347\nEUR',
        }
        self.expected_results = ('PLN', 'EUR', '4.3307', '2019-02-15T13:15:00Z')

    def test_get_currency_rates(self):
        """
        Testing method retrieving data from rss feed
        """

        result = get_currency_rates(self.rss_url)

        self.assertTrue(isinstance(result, dict))

        # TODO add more asserts testing result content

    def test_parse_currency_rate_data(self):
        """
        Testing method responsible for parsing data to database acceptable format
        """

        parsed_exchange = parse_currency_rate_data(self.piece_of_currency_rate_data)

        self.assertEqual(len(self.expected_results), len(parsed_exchange))
        for i in range(len(self.expected_results)):
            self.assertTrue(isinstance(parsed_exchange[i], type(self.expected_results[i])))

    # TODO add more tests for parse_currency_rate_data method for lack of attribute or wrong format
