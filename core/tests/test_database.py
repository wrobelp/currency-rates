from django.test import TestCase

from core.models import ExchangeRate
from core.utils import add_exchange_rate_to_database


class SaveCurrencyRatesToDatabaseTests(TestCase):

    def setUp(self):
        # TODO purge database

        self.expected_results = ('PLN', 'EUR', '4.3307', '2019-02-15T13:15:00Z')

    def tearDown(self):
        # TODO purge database
        pass

    def test_add_exchange_rate_to_database(self):
        self.assertEqual(ExchangeRate.objects.all().count(), 0)

        add_exchange_rate_to_database(*self.expected_results)

        self.assertGreater(ExchangeRate.objects.all().count(), 0)
