import feedparser


def get_currency_rates(url):
    """
    Method retrieving data from rss feed

    :param url: feed url
    :return: dict with data
    """

    return feedparser.parse(url)


def parse_currency_rate_data(item):
    """
    Method responsible for parsing data to database acceptable format

    :param item: piece of data representing one exchange rate
    :return:
    base, target: currency codes ex. 'PLN', 'EU';
    rate: currency exchange rate value ex. '4.1222'
    updated: date and time of rate update ex. '2019-02-15T13:15:00Z'
    """

    if all(k in item for k in ('cb_exchangerate', 'cb_targetcurrency', 'updated')):
        rate, base = item['cb_exchangerate'].split('\n')
        target = item['cb_targetcurrency']
        updated = item['updated']
        return base, target, rate, updated
    return None


def add_exchange_rate_to_database(base, target, rate, updated):
    pass
